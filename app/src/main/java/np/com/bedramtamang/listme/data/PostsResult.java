package np.com.bedramtamang.listme.data;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostsResult {

    @SerializedName("articles")
    private List<Post> mPosts;
    @SerializedName("totalResults")
    private int total_results;

    public List<Post> getmPosts() {
        return mPosts;
    }

    public void setmPosts(List<Post> mPosts) {
        this.mPosts = mPosts;
    }
}
