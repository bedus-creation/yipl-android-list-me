package np.com.bedramtamang.listme.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Posts {

    @SerializedName("totalResults")
    private int totalArticleLength;


    @SerializedName("articles")
    private List<Post> posts;

    public Posts(int length,List<Post> mPosts){
        this.totalArticleLength=length;
        this.posts=mPosts;
    }

    public List<Post> getPosts() {
        return this.posts;
    }

}
