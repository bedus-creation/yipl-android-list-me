package np.com.bedramtamang.listme.data;

import com.google.gson.annotations.SerializedName;

public class Post {
    @SerializedName("url")
    private String url;

    @SerializedName("author")
    private String author;

    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String descrtiption;

    @SerializedName("content")
    private String content;

    @SerializedName("urlToImage")
    private String urlToImage;

    @SerializedName("publishedAt")
    private String publishedAt;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescrtiption() {
        return descrtiption;
    }

    public void setDescrtiption(String descrtiption) {
        this.descrtiption = descrtiption;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public void setUrlToImage(String urlToImage) {
        this.urlToImage = urlToImage;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Post(String url, String author, String title, String description, String content,
                String urlToImage, String publishedAt){
        this.author=author;
        this.descrtiption=description;
        this.title=title;
        this.content=content;
        this.urlToImage=urlToImage;
        this.publishedAt=publishedAt;
        this.url=url;
    }
}
