package np.com.bedramtamang.listme.rest;

import np.com.bedramtamang.listme.data.Posts;
import np.com.bedramtamang.listme.data.PostsResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface ApiInterface {
    @GET("v2/top-headlines?country=us&category=business&apiKey=2aecbef611824c65b3d1e887299cc9ea")
    Call<Posts> getAllPosts();

    @GET("posts/{id}/comments")
    Call<PostsResult> getPostDetails(@Path("id") int id);
}
