package np.com.bedramtamang.listme.rest;

import android.util.Log;

import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;


public class ApiClient {

    public static final String BASE_URL = "https://newsapi.org/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        Log.d("RETROFIT","THE URL IS IS ACCESSING IS: "+retrofit.toString());
        return retrofit;
    }
}
