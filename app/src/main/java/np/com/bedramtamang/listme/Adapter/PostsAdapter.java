package np.com.bedramtamang.listme.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import np.com.bedramtamang.listme.R;
import np.com.bedramtamang.listme.data.Post;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.EachPostHolder>{

    final private ListItemClickListener mOnClickListItemClickListener;

    private List<Post> post;
    private int rowLayout;
    private Context context;

    public PostsAdapter(List<Post> post,ListItemClickListener listner, int rowLayout, Context context) {
        this.post = post;
        this.rowLayout = rowLayout;
        this.context = context;
        this.mOnClickListItemClickListener=listner;
    }
    public interface ListItemClickListener{
        void onListItemClick(String ClickedItemIndex);
    }

    @Override
    public EachPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new EachPostHolder(view);
    }

    @Override
    public int getItemCount() {
        return post.size();
    }

    @Override
    public void onBindViewHolder(EachPostHolder holder, int position) {
        holder.title.setText(post.get(position).getTitle());
        holder.body.setText(post.get(position).getDescrtiption());
        holder.post_Id.setText(""+post.get(position).getPublishedAt());
        Picasso.with(context).load(post.get(position).getUrlToImage()).into(holder.coverImage);
        holder.url=post.get(position).getUrl();
    }

    class EachPostHolder extends RecyclerView.ViewHolder implements
    View.OnClickListener{
        TextView title;
        TextView body;
        TextView post_Id;
        ImageView coverImage;
        String url;
        public EachPostHolder(View itemView) {
            super(itemView);
            title=(TextView) itemView.findViewById(R.id.title);
            body= (TextView) itemView.findViewById(R.id.description);
            post_Id=(TextView) itemView.findViewById(R.id.time);
            coverImage=(ImageView) itemView.findViewById(R.id.coverImage);

            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            int clickedPosition=getAdapterPosition();
            String post_id=post_Id.getText().toString();
            mOnClickListItemClickListener.onListItemClick(url);
        }
    }
}
