package np.com.bedramtamang.listme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import np.com.bedramtamang.listme.data.Posts;
import np.com.bedramtamang.listme.rest.ApiClient;
import np.com.bedramtamang.listme.rest.ApiInterface;
import retrofit2.Call;

public class DetailsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        String url = intent.getStringExtra(MainActivity.SET_URL);

        WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.loadUrl(url);

    }
}
