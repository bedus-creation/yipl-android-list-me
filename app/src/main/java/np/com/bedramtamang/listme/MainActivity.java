package np.com.bedramtamang.listme;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import np.com.bedramtamang.listme.Adapter.PostsAdapter;
import np.com.bedramtamang.listme.data.Post;
import np.com.bedramtamang.listme.data.Posts;
import np.com.bedramtamang.listme.data.PostsResult;
import np.com.bedramtamang.listme.rest.ApiClient;
import np.com.bedramtamang.listme.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements PostsAdapter.ListItemClickListener{

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String SET_URL = "url";

    RecyclerView mRecyclerViewForPosts;
    ProgressDialog progress;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        this.setUpSwiftRefresh();

        this.setUpRecycleVIew();

        progress = ProgressDialog.show(this, "Fetching Data",
                "Loading....", true);

        this.getDataFromNetwork();

        progress.dismiss();


    }
    @Override
    public void onListItemClick(String ClickedItemIndex) {
        Intent intent=new Intent(this,DetailsActivity.class);
        intent.putExtra(SET_URL,ClickedItemIndex);
        startActivity(intent);
    }

    /**
     * Perform Refresh when SwiftRefresh
     */
    private void setUpSwiftRefresh(){

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.simpleSwipeRefreshLayout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // cancel the Visual indication of a refresh
                swipeRefreshLayout.setRefreshing(false);
                getDataFromNetwork();
            }
        });
    }

    /**
     * Setup RecycleView
     */
    private void setUpRecycleVIew(){
        mRecyclerViewForPosts=(RecyclerView) findViewById(R.id.rv_posts);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        mRecyclerViewForPosts.setLayoutManager(linearLayoutManager);
    }

    /**
     * This function wrap the network stuff with retrofit Api
     * It fetch the data and sets to the Posts class and render with recycleView
     */
    private void getDataFromNetwork(){

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<Posts> call=apiService.getAllPosts();

        call.enqueue(new Callback<Posts>() {
            @Override
            public void onResponse(Call<Posts> call, Response<Posts> response) {

                List<Post> posts = response.body().getPosts();
                PostsResult postsResult=new PostsResult();
                postsResult.setmPosts(posts);
                mRecyclerViewForPosts.setAdapter(new PostsAdapter(posts,MainActivity.this,R.layout.each_post_layout,getApplicationContext()));
                Log.e(TAG, "Number posts received: "+postsResult.getmPosts());
            }

            @Override
            public void onFailure(Call<Posts> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }
}
